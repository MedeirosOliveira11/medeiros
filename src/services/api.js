import axios from 'axios';

// Base da URL:https://api.themoviedb.org/3/
// URL DA API:https://api.themoviedb.org/3/movie/now_playing?api_key=f4e3f1ac7488cc986d56d2eb9b24c938&language=pt-BR


const api = axios.create({
    baseURL: 'https://api.themoviedb.org/3/'
});

export default api;