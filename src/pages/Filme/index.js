import { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import './filme-info.css';
import api from '../../services/api';
import { toast} from 'react-toastify'



function Filme(){
    const {id}= useParams();
    const navigation = useNavigate ();

    const [filme, setFilme] = useState({});
    const [loading, setLoading] = useState(true);

    useEffect(()=>{
        async function loadFilme(){
            await api.get(`/movie/${id}`, {
                params:{
                    api_key:"f4e3f1ac7488cc986d56d2eb9b24c938",
                    language:"pt-BR"
                }
            })
            .then((response)=>{
              setFilme(response.data);
              setLoading(false);

            })
            .catch(()=>{
                console.log('FILME NÃO ENCONTRADO');
                navigation("/",  {reaplace: true});
                return;
            })
        }

        loadFilme();

        return() => {

            console.log("Desmontado");
        }

    }, [navigation, id])


    function salvarfilme(){
       const minhalista = localStorage.getItem("@medeirosflix");

       let filmesSalvos = JSON.parse(minhalista) || [];

    
       const hasFilme = filmesSalvos.some((filmesSalvo) => filmesSalvo.id === filme.id)

        if(hasFilme){
            toast.warn("Esse filme já está na sua lista!")
           
            return;
        }

        filmesSalvos.push(filme);
        localStorage.setItem ("@medeirosflix", JSON.stringify(filmesSalvos));
        toast.success("Filme salvo com sucesso")
        
    }



    if(loading){
        return(
            <div className="filme-info">
                <h1>Carregando Detalhes.....</h1>

            </div>
        )
    }


    return(
        <div className="filme-info">
            <h1>{filme.title}</h1>
            <img src={`https://image.tmdb.org/t/p/original/${filme.backdrop_path}`} alt={filme.title}/> 
            
            <h3>Sinopse</h3>
            <span>{filme.overview}</span>
            <strong>Avaliação: {filme.vote_average} / 10</strong>

        <div className="area-buttons">
        <button onClick={salvarfilme}>Salvar</button>
        <button>
            <a  target="blank" href= {`https://youtube.com/results?search_query=${filme.title} Trailer`}>
                Trailer
            </a>
        </button>
        </div>

        </div>
    )
}

export default Filme;